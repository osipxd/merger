Feature: Time and chapter parser
  First cell in a table has following format: [chapter] time
  Where "chapter" has free format and "time" has format [hh:]mm:ss[.SSS]

  Scenario Outline: Parser normalizes time
    Given I have time "<time>"
    When I normalize the time
    Then I should get time "<formatted>"

    Examples:
      | time      | formatted    |
      | 1:45      | 00:01:45.000 |
      | 00:34     | 00:00:34.000 |
      | 01:52:10  | 01:52:10.000 |
      | 00:11.933 | 00:00:11.933 |

  @wip
  Scenario: Parser parses one cell with simplest format
    Given I have cell "[1] 01:56"
    When I parse the cell
    Then I should get chapter "1"
    And I should get time "00:01:56.000"
