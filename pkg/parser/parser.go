package parser

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var trimRegex = regexp.MustCompile(`[:.]`)

func NormalizeTime(rawTime string) (string, error) {
	trimmed := trimRegex.ReplaceAllString(rawTime, "")
	if !strings.Contains(rawTime, ".") {
		trimmed += "000"
	}
	num, err := strconv.Atoi(trimmed)
	if err != nil {
		return "", err
	}

	const CommonPart, MillisecondsPart = 100, 1000
	result := fmt.Sprintf(".%03d", num%MillisecondsPart)
	num /= MillisecondsPart

	for i := 0; i < 3; i++ {
		if i != 0 {
			result = ":" + result
		}
		result = fmt.Sprintf("%02d", num%CommonPart) + result
		num /= CommonPart
	}

	return result, nil
}
