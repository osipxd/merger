package parser

import (
	"fmt"
	"github.com/DATA-DOG/godog"
	"os"
	"testing"
)

var rawTime string
var time string

func iHaveTime(time string) error {
	rawTime = time
	return nil
}

func iHaveCell(_ string) error {
	return godog.ErrPending
}

func iNormalizeTheTime() error {
	var err error
	time, err = NormalizeTime(rawTime)
	return err
}

func iParseTheCell() error {
	return godog.ErrPending
}

func iShouldGetTime(value string) error {
	if value != time {
		return fmt.Errorf("expected %s but given %s", value, time)
	}
	return nil
}

func iShouldGetChapter(value string) error {
	if value != time {
		return fmt.Errorf("expected %s but given %s", value, time)
	}
	return nil
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^I have time "([^"]*)"$`, iHaveTime)
	s.Step(`^I have cell "([^"]*)"$`, iHaveCell)
	s.Step(`^I normalize the time$`, iNormalizeTheTime)
	s.Step(`^I parse the cell$`, iParseTheCell)
	s.Step(`^I should get time "([^"]*)"$`, iShouldGetTime)
	s.Step(`^I should get chapter "([^"]*)"$`, iShouldGetChapter)
}

func TestMain(m *testing.M) {
	format := "progress"
	for _, arg := range os.Args[1:] {
		if arg == "-test.v=true" { // go test transforms -v option
			format = "pretty"
			break
		}
	}
	status := godog.RunWithOptions("godog", func(s *godog.Suite) {
		godog.SuiteContext(s)
	}, godog.Options{
		Format: format,
		Paths: []string{"features"},
	})

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}
